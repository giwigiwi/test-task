﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathDrawer : MonoBehaviour
{
    private PlayerInput _playerInput;
    [SerializeField]
    private LineRenderer _lineRenderer;
    
    
    void Awake()
    {
        _playerInput = GameManager.instance.playerInput;
    }
    
    void Update()
    {
        if (_playerInput.path.Capacity > 0)
        {
            DrawPath(_playerInput.path.ToArray());
        }
    }

    public void SetPlayerAsFirstPoint(Vector3 point)
    {
        _lineRenderer.SetPosition(0,point);   
    }

    private void DrawPath(PathPoint[] points)
    {
        _lineRenderer.positionCount = points.Length+1;
        for (int i = 0; i < points.Length; i++)
        {
            _lineRenderer.SetPosition(i+1,points[i].transform.position);
        }

    }
}
