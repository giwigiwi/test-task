﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public PlayerInput playerInput;
    public CharacterController characterController;
    public PathDrawer drawer;
    public UiManager uiManager;

    void Awake()
    {
        instance = this;
    }
    
    void Update()
    {
        drawer.SetPlayerAsFirstPoint(new Vector3(characterController.transform.position.x,0,characterController.transform.position.z));
        if (playerInput.path.Count > 0)
        {
            characterController.MoveToNextPoint();
        }

    }
}
