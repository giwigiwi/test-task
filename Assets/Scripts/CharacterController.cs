﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    
    public float speed;
    
    void Update()
    {
        speed = GameManager.instance.uiManager.speedSlider.value;
    }
    
    
    public void MoveToNextPoint()
    {
        //transform.LookAt(GameManager.instance.playerInput.path[0].transform.position);
        transform.position = Vector3.MoveTowards(transform.position, GameManager.instance.playerInput.path[0].transform.position, speed * Time.deltaTime);
    }
}
