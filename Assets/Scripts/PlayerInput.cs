﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private Vector3 _pos;
    public List<PathPoint> path;
    public PathPoint point;
    

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.transform.CompareTag("Ground"))
                {
                    _pos = hit.point;
                    path.Add(Instantiate(point, _pos, Quaternion.identity));
                }
            }
        }
    }
    
    
}
