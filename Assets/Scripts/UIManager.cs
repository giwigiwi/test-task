﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public Slider speedSlider;
    public Text speedSliderValue;
    
    
    void Update()
    {
        speedSliderValue.text = String.Format("{0:F1}",speedSlider.value);
    }
}
