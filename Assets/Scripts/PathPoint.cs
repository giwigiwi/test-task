﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPoint : MonoBehaviour
{

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position,0.2f);
    }

    private void OnTriggerEnter(Collider other)
    {
        GameManager.instance.playerInput.path.Remove(this);
        Destroy(gameObject);
    }
}
